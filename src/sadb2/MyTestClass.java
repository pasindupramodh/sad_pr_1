/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sadb2;

import com.sadb2.db.DB;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author lashanc
 * This class used to bla bla
 */
public class MyTestClass {
    /**
     * This method used to get Employee's full name
     * @param fname Emplyee's first name
     * @param lname Employee's last name
     * @return employee fullname
     */
    public String getEmployeeFullName(String fname,String lname){
        System.out.println("A");
        return fname + " "+ lname;
    }
    
    public static void main(String[] args) {
        try {
            ResultSet rs = DB.search("select * from employee");
            while (rs.next()) {                
                System.out.println(rs.getString("fname")+" "+rs.getString("lname"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
