/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sadb2.dto;

/**
 *
 * @author ASUS
 */
public class InvoiceItemRow {
    private double qty;
    private double total;
    public void setQty(double qty) {
        this.qty = qty;
    }

    public void setTotal(double total) {
        this.total = total;
    }
    

    public double getQty() {
        return qty;
    }

    public double getTotal() {
        return total;
    }
    
}
