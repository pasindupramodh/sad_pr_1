/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sadbatch2;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 *
 * @author Lashan
 */
public class SADBATCH2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String yours = "‪C:\\Users\\bobdu\\eclipse-workspace\\SHIPTesting.txt";
        String retyp = "C:\\Users\\bobdu\\eclipse-workspace\\SHIPTesting.txt";
        System.out.println("yours len="+yours.length()+", retype=" + retyp.length());
        try {
            System.out.println(convertToBinary(yours,"ISO-8859-13"));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(SADBATCH2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    static String convertToBinary(String input, String encoding) 
      throws UnsupportedEncodingException {
    byte[] encoded_input = Charset.forName(encoding)
      .encode(input)
      .array();  
    return IntStream.range(0, encoded_input.length)
        .map(i -> encoded_input[i])
        .mapToObj(e -> Integer.toBinaryString(e ^ 255))
        .map(e -> String.format("%1$" + Byte.SIZE + "s", e).replace(" ", "0"))
        .collect(Collectors.joining(" "));
}
}
